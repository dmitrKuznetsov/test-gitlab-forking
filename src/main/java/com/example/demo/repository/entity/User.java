package com.example.demo.repository.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name = "user_")
@NoArgsConstructor
@AllArgsConstructor
public class User {

  @Id
  private String email;

  private String password;
}
